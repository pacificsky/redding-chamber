<?php
/**
 * Header file common to all
 * templates
 *
 */
 $sponsor = get_posts(array('post_type' => 'chamber_partners', 'posts_per_page' => 1, 'partner-category' => 'sponsor'))[0];
?>
<!doctype html>
<html class="site no-js" <?php language_attributes(); ?>>
<head>
	<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
	<![endif]-->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href='https://fonts.googleapis.com/css?family=Lato:100,400,700,700italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon-194x194.png" sizes="194x194">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/manifest.json">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/safari-pinned-tab.svg" color="#1c5d74">
	<meta name="msapplication-TileColor" content="#1c5d74">
	<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<title><?php wp_title(); ?></title>

	<?php // replace the no-js class to js on the html element ?>
	<script>document.documentElement.className=document.documentElement.className.replace(/\sno-js\s/,'js')</script>

	<?php // load the core js polyfills ?>
	<script async defer src="<?php echo get_template_directory_uri(); ?>/assets/js/core.js"></script>

	<?php wp_head(); ?>
</head>
<body <?php body_class( 'site__body' ); ?>>

	<?php //if(current_user_can('manage_options')) edit_post_link(); ?>

	<header class="site__header">
		<?php
		ob_start(); ?>
		<div id="header_social">
	    <a href="https://www.facebook.com/ReddingChamberofCommerce/"><?php MOZ_SVG::svg('facebook'); ?></a>
	    <a href="https://twitter.com/ReddingChamber1"><?php MOZ_SVG::svg('twitter'); ?></a>
	    <a href="https://plus.google.com/107595056307311633205/about"><?php MOZ_SVG::svg('google-plus'); ?></a>
	    <a href="https://www.instagram.com/explore/tags/reddingchamber/"><?php MOZ_SVG::svg('instagram'); ?></a>
	  </div>
		<ul class="menu__list">%3$s</ul>
		<?php
		$header_wrap = ob_get_clean();
		MOZ_Menu::nav_menu( 'top', array(
			'items_wrap' => $header_wrap
		) );
		?>
		<a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="" id="logo" /><?php MOZ_SVG::svg( 'logo' ); ?></a>
		<!-- <a id="sponsor" href="<?php echo get_the_permalink($sponsor->ID); ?>" target="_blank"><span>Sponsored by</span><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($sponsor->ID)); ?>" /></a> -->
		<a href="https://reddingcacoc.wliinc17.com/join" id="join_now">Join Now</a>
		<?php MOZ_Menu::nav_menu( 'primary' ); ?>
	</header>
