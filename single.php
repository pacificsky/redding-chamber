<?php
/**
 * Read up on the WP Template Hierarchy for
 * when this file is used
 *
 */
 $parents = get_post_ancestors(get_the_ID());
 $id = ($parents) ? $parents[count($parents) - 1] : $post->ID;
 $parent = get_post( $id );
 $slug = $parent->post_name;
?>
<?php get_header(); ?>

	<h1 class="page__title"><?php the_title(); ?><?php if($slug === 'business-solutions-center'): ?> <span id="us_bank">presented by <img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/2016/05/US-Bank-Logo.png"></span><?php endif; ?></h1>
	<main class="page__content">
		<div class="page__content__body">
      <?php echo wpautop(do_shortcode(get_the_content())); ?>
      <?php if($slug === 'business-solutions-center'): ?>
        <div id="business-solutions">
        <?php $children = get_children(array( 'post_parent' => get_the_ID(), 'numberposts' => -1, 'post_status' => 'publish'));
        foreach ($children as $child):
          ?>
            <a href="<?php echo get_the_permalink($child->ID); ?>" style="background-image: url('<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($child->ID), 'medium')[0]; ?>')" class="business-solution">
              <h2><?php echo $child->post_title; ?></h2>
            </a>
          <?php
        endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
    <aside class="sidebar">
  		<?php if($slug === 'about-the-chamber'): ?>
  			<?php MOZ_Menu::nav_menu('about'); ?>
  		<?php elseif($slug === 'resources'): ?>
  			<?php MOZ_Menu::nav_menu('resources'); ?>
      <?php elseif($slug === 'contact-us'): ?>
        <?php dynamic_sidebar('contact-us'); ?>
  		<?php endif; ?>
      <h3>Upcoming Events</h3>
      <hr>
      <?php echo do_shortcode('[chamber_events_feed limit="2"]'); ?>
      <h3>Premier Partners</h3>
      <hr>
      <?php
    	$premier_partners = get_posts(array(
    		'post_type' => 'chamber_partners',
    		'posts_per_page' => -1,
    		'partner-category' => 'premier'
    	));
    	foreach ($premier_partners as $i => $partner) {
    		?>
    		<figure class="partner">
    			<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($partner->ID)); ?>" alt="" />
    		</figure>
    		<?php
        if($i !== count($premier_partners) - 1) echo "<hr>";
    	}
    	 ?>
    </aside>
		<div class="join_now">
			<a href="https://reddingcacoc.wliinc17.com/join" class="button">Join Us</a>
		</div>
	</main>

<?php get_footer(); ?>
