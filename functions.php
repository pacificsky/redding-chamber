<?php
/**
 * Theme Functions &
 * Functionality
 *
 */


/* =========================================
		ACTION HOOKS & FILTERS
   ========================================= */

/**--- Actions ---**/

add_action( 'after_setup_theme',  'theme_setup' );

add_action( 'wp_enqueue_scripts', 'theme_styles' );

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

// expose php variables to js. just uncomment line
// below and see function theme_scripts_localize
// add_action( 'wp_enqueue_scripts', 'theme_scripts_localize', 20 );

/**--- Filters ---**/



/* =========================================
		HOOKED Functions
   ========================================= */

/**--- Actions ---**/


/**
 * Setup the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_setup' ) ) {
	function theme_setup() {

		// Let wp know we want to use html5 for content
		add_theme_support( 'html5', array(
			'comment-list',
			'comment-form',
			'search-form',
			'gallery',
			'caption'
		) );


		add_theme_support( 'post-thumbnails' );


		// Register navigation menus for theme
		register_nav_menus( array(
			'top' => 'Top Menu',
			'primary' => 'Main Menu',
			'about' => 'About Pages Side Menu',
			'resources' => 'Resource Pages Side Menu'
		) );


		register_sidebar(array(
			'name' => 'Footer',
			'id' => 'footer',
			'description' => 'Footer Widgets',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			 'after_widget'  => '</div>',
			 'before_title'  => '<h2 class="widgettitle">',
			 'after_title'   => '</h2>',
		));

		register_sidebar(array(
			'name' => 'Contact Us',
			'id' => 'contact-us',
			'description' => 'Contact Us Page Sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			 'after_widget'  => '</div>',
			 'before_title'  => '<h2 class="widgettitle">',
			 'after_title'   => '</h2>',
		));

		add_filter( 'widget_text', 'shortcode_unautop' );
		add_filter( 'widget_text', 'do_shortcode' );


		// Let wp know we are going to handle styling galleries
		/*
		add_filter( 'use_default_gallery_style', '__return_false' );
		*/


		// Stop WP from printing emoji service on the front
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );


		// Remove toolbar for all users in front end
		// show_admin_bar( false );


		// Add Custom Image Sizes
		/*
		add_image_size( 'ExampleImageSize', 1200, 450, true ); // Example Image Size
		...
		*/


		// WPML configuration
		// disable plugin from printing styles and js
		// we are going to handle all that ourselves.
		define( 'ICL_DONT_LOAD_NAVIGATION_CSS', true );
		define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );
		define( 'ICL_DONT_LOAD_LANGUAGES_JS', true );


		// Contact Form 7 Configuration needs to be done
		// in wp-config.php. add the following snippet
		// under the line:
		// define( 'WP_DEBUG', false );
		/*
		//Contact Form 7 Plugin Configuration
		define ( 'WPCF7_LOAD_JS',  false ); // Added to disable JS loading
		define ( 'WPCF7_LOAD_CSS', false ); // Added to disable CSS loading
		define ( 'WPCF7_AUTOP',    false ); // Added to disable adding <p> & <br> in form output
		*/


		// Register Autoloaders Loader
		$theme_dir = get_template_directory();
		include "$theme_dir/library/library-loader.php";
		include "$theme_dir/includes/includes-loader.php";
		include "$theme_dir/components/components-loader.php";
	}
}


/**
 * Register and/or Enqueue
 * Styles for the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_styles' ) ) {
	function theme_styles() {
		$theme_dir = get_stylesheet_directory_uri();

		wp_enqueue_style( 'main', "$theme_dir/assets/css/main.css", array(), null, 'all' );
	}
}


/**
 * Register and/or Enqueue
 * Scripts for the theme
 *
 * @since 1.0
 */
if ( ! function_exists( 'theme_scripts' ) ) {
	function theme_scripts() {
		$theme_dir = get_stylesheet_directory_uri();

		wp_enqueue_script( 'main', "$theme_dir/assets/js/main.js", array( 'jquery' ), null, true );
	}
}


/**
 * Attach variables we want
 * to expose to our JS
 *
 * @since 3.12.0
 */
if ( ! function_exists( 'theme_scripts_localize' ) ) {
	function theme_scripts_localize() {
		$ajax_url_params = array();

		// You can remove this block if you don't use WPML
		if ( function_exists( 'wpml_object_id' ) ) {
			/** @var $sitepress SitePress */
			global $sitepress;

			$current_lang = $sitepress->get_current_language();
			wp_localize_script( 'main', 'i18n', array(
				'lang' => $current_lang
			) );

			$ajax_url_params['lang'] = $current_lang;
		}

		wp_localize_script( 'main', 'urls', array(
			'home'  => home_url(),
			'theme' => get_stylesheet_directory_uri(),
			'ajax'  => add_query_arg( $ajax_url_params, admin_url( 'admin-ajax.php' ) )
		) );
	}
}


// Register Custom Post Type
function chamber_register_post_type_partners() {

	$labels = array(
		'name'                  => 'Partners',
		'singular_name'         => 'Partner',
		'menu_name'             => 'Partners',
		'name_admin_bar'        => 'Partner',
		'archives'              => 'Partner Archives',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Partners',
		'add_new_item'          => 'Add New Partner',
		'add_new'               => 'Add New',
		'new_item'              => 'New Partner',
		'edit_item'             => 'Edit Partner',
		'update_item'           => 'Update Partner',
		'view_item'             => 'View Partner',
		'search_items'          => 'Search Partner',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into partner',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Partners list',
		'items_list_navigation' => 'Partners list navigation',
		'filter_items_list'     => 'Filter partners list',
	);
	$args = array(
		'label'                 => 'Partner',
		'description'           => 'Partners of the Redding Chamber of Commerce',
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'partner-category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'chamber_partners', $args );

}
add_action( 'init', 'chamber_register_post_type_partners', 0 );

// Register Custom Taxonomy
function chamber_register_taxonomy_partner_category() {

	$labels = array(
		'name'                       => 'Partner Categories',
		'singular_name'              => 'Partner Category',
		'menu_name'                  => 'Partner Categories',
		'all_items'                  => 'All Partner Categories',
		'parent_item'                => 'Parent Partner Category',
		'parent_item_colon'          => 'Parent Item:',
		'new_item_name'              => 'New Partner Category Name',
		'add_new_item'               => 'Add New Partner Category',
		'edit_item'                  => 'Edit Partner Category',
		'update_item'                => 'Update Partner Category',
		'view_item'                  => 'View Partner Category',
		'separate_items_with_commas' => 'Separate partner categories with commas',
		'add_or_remove_items'        => 'Add or remove partner categories',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Partner Categories',
		'search_items'               => 'Search Partner Categories',
		'not_found'                  => 'Not Found',
		'no_terms'                   => 'No partner categories',
		'items_list'                 => 'Partner categories list',
		'items_list_navigation'      => 'Partner categories list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'partner-category', array( 'chamber_partners' ), $args );

}
add_action( 'init', 'chamber_register_taxonomy_partner_category', 0 );

function avada_process_tag( $m ) {
   if ($m[2] == 'dropcap' || $m[2] == 'highlight' || $m[2] == 'tooltip') {
      return $m[0];
   }

	// allow [[foo]] syntax for escaping a tag
	if ( $m[1] == '[' && $m[6] == ']' ) {
		return substr($m[0], 1, -1);
	}

   return $m[1] . $m[6];
}


function tf_content($limit, $strip_html) {
	global $data, $more;

	if(!$limit && $limit != 0) {
		$limit = 285;
	}

	$limit = (int) $limit;

	$test_strip_html = $strip_html;

	if($strip_html == "true" || $strip_html == true) {
		$test_strip_html = true;
	}

	if($strip_html == "false" || $strip_html == false) {
		$test_strip_html = false;
	}

	$custom_excerpt = false;

	$post = get_post(get_the_ID());

	$pos = strpos($post->post_content, '<!--more-->');

	if($data['link_read_more']) {
		$readmore = ' <a href="'.get_permalink( get_the_ID() ).'">&#91;...&#93;</a>';
	} else {
		$readmore = ' &#91;...&#93;';
	}

	if($data['disable_excerpts']) {
		$readmore = '';
	}

	if($test_strip_html) {
		$raw_content = strip_tags( get_the_content($readmore) );
		if($post->post_excerpt || $pos !== false) {
			$more = 0;
			$raw_content = strip_tags( get_the_content($readmore) );
			$custom_excerpt = true;
		}
	} else {
		$raw_content = get_the_content($readmore);
		if($post->post_excerpt || $pos !== false) {
			$more = 0;
			$raw_content = get_the_content($readmore);
			$custom_excerpt = true;
		}
	}

	if($raw_content && $custom_excerpt == false) {
		$content = $raw_content;

		if($test_strip_html == true) {
			$pattern = get_shortcode_regex();
			$content = preg_replace_callback("/$pattern/s", 'avada_process_tag', $content);
		}
		$content = explode(' ', $content, $limit);
		if(count($content)>=$limit) {
			array_pop($content);
			if($data['disable_excerpts']) {
				$content = implode(" ",$content);
			} else {
				$content = implode(" ",$content);
			if($limit != 0) {
					if($data['link_read_more']) {
						$content .= $readmore;
					} else {
						$content .= $readmore;
					}
				}
			}
		} else {
			$content = implode(" ",$content);
		}

		if( $limit != 0 ) {
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
		}

		$content = '<div class="excerpt-container">'.do_shortcode($content).'</div>';

		return $content;
	}

	if($custom_excerpt == true) {
		$content = $raw_content;
		if($test_strip_html == true) {
			$pattern = get_shortcode_regex();
			$content = preg_replace_callback("/$pattern/s", 'avada_process_tag', $content);
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			$content = '<div class="excerpt-container">'.do_shortcode($content).'</div>';
		} else {
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
		}
	}

	if(has_excerpt()) {
		$content = do_shortcode(get_the_excerpt());
	}

	return $content;
}


function chamber_get_events_feed() {
	include_once get_stylesheet_directory() . "/library/Rss_Item.php";
	include_once get_stylesheet_directory() . "/library/Rss_Parser.php";
	$return = array();
	$feed = new Rss_Parser();
	$feed->load('http://web.reddingchamber.com/events?RSS=True');
	$regex_date = '/[0-3]?[0-9]\/[0-3]?[0-9]\/(?:[0-9]{2})?[0-9]{2}/';
	foreach ($feed->getItems() as $item) {
		preg_match($regex_date, $item->getTitle(), $matches);
		preg_match_all('/([0-9]?[0-9]:[0-9][0-9] (AM|PM))|Noon/', $item->getTitle(), $dateMatches);
		if($matches[0]) {
			$dateDay = $matches[0];
			$dateIndex = strpos($item->getTitle(), $dateDay);
			$item->setTitle(substr($item->getTitle(), 0, $dateIndex - 1));
			if($dateMatches[0]) {
				$dateMatches = $dateMatches[0];
				if($dateMatches[0] === 'Noon') $dateMatches[0] = '12:00 PM';
				if($dateMatches[1] === 'Noon') $dateMatches[1] = '12:00 PM';
				$item->dateStart = strtotime($dateDay . ' ' . $dateMatches[0]);
				$item->dateEnd = strtotime($dateDay . ' ' . $dateMatches[1]);
			} else {
				$item->dateStart = strtotime($matches[0]);
				$item->dateEnd = strtotime($matches[0]);
			}
		}
		$return[] = $item;
	}
	return $return;
}

function chamber_shortcode_events_feed($atts) {
	$atts = extract(shortcode_atts(array('limit'=>10),$atts));
	ob_start();
	$events = array_slice(chamber_get_events_feed(), 0, $limit);
	echo "<ul class='events'>";
	foreach($events as $event): ?>
		<li><a href="<?php echo $event->getLink(); ?>">
			<date>
				<span class="month"><?php echo date('M', $event->dateStart); ?></span>
				<span class="day"><?php echo date('j', $event->dateStart); ?></span>
			</date>
			<h3><?php echo $event->getTitle(); ?></h3>
			<?php if(date('g:i A', $event->dateStart) === '12:00 AM' && date('g:i A', $event->dateEnd) === '12:00 AM'): ?>
				<time>All Day</time>
			<?php else: ?>
				<time><?php echo date('g:i A', $event->dateStart) . ' - ' . date('g:i A', $event->dateEnd); ?></time>
			<?php endif; ?>
		</a></li>
	<?php endforeach;
	echo "</ul>";
	return ob_get_clean();
}
add_shortcode('chamber_events_feed', 'chamber_shortcode_events_feed');

function chamber_shortcode_featured_member($atts) {
	ob_start();
	?>

	<div id="featured_partner">
	<?php
	$featured_member = get_posts(array(
		'post_type' => 'chamber_partners',
		'posts_per_page' => 1,
		'partner-category' => 'featured'
	))[0];


	 ?>
	 <figure class="partner">
		 <span style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($featured_member->ID)); ?>)" class="image"></span>
		 <h3><?php echo $featured_member->post_title; ?></h3>
		 <p>
			<?php echo $featured_member->post_content; ?>
		 </p>
	 </figure>
	 </div>

	<?php

	return ob_get_clean();
}
add_shortcode('chamber_featured_member', 'chamber_shortcode_featured_member');




add_action('load-post.php', 'chamber_meta_boxes_setup');
add_action('load-post-new.php', 'chamber_meta_boxes_setup');

function chamber_meta_boxes_setup() {
	add_action('add_meta_boxes', 'chamber_add_partner_meta_boxes');
	add_action('save_post', 'chamber_save_partner_meta', 10, 2);
}

function chamber_add_partner_meta_boxes() {
	add_meta_box('chamber_partner_meta', 'Partner Information', 'chamber_partner_meta_cb', 'chamber_partners', 'normal', 'high');
}

function chamber_save_partner_meta($post_id, $post) {
	if ( !isset( $_POST['chamber_partner_meta_nonce'] ) || !wp_verify_nonce( $_POST['chamber_partner_meta_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	$post_type = get_post_type_object( $post->post_type );

	 if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	if( isset( $_POST[ '_chamber_partner_url'] ) ) {
		update_post_meta( $post_id, '_chamber_partner_url', $_POST['_chamber_partner_url'] );
	}
}


function chamber_partner_meta_cb($post) {
	wp_nonce_field(basename(__FILE__), 'chamber_partner_meta_nonce');

	ob_start();

	?>

	<label for="_chamber_partner_url">Website</label>
	 <br />
	 <input type="text" class="widefat" name="_chamber_partner_url" id="meta_chamber_partner_url" value="<?php echo esc_attr( get_post_meta( $post->ID, "_chamber_partner_url", true ) ); ?>">
	 <hr />

	 <?php

 echo ob_get_clean();
}
