<?php
/**
 * Footer file common to all
 * templates
 *
 */
?>

<!-- <div id="map"></div>
<script src='https://api.mapbox.com/mapbox-gl-js/v0.18.0/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v0.18.0/mapbox-gl.css' rel='stylesheet' />
<script>
mapboxgl.accessToken = 'pk.eyJ1IjoidHlsZXJzaHVzdGVyIiwiYSI6IkVtQUNDR0kifQ.lmw4IoRwovnroo6vfHO9gQ';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/tylershuster/cioevihyr002da5nemecmlnvj'
});
var latLng = [-122.381223,40.587627];
map.setZoom(13.6);
map.setCenter(latLng);
var mainOfficeDiv = document.createElement('div');
mainOfficeDiv.id = 'markerMain';
mainOfficeDiv.innerHTML = "";
mainOfficeDiv.className = "mapboxMarker";
new mapboxgl.Popup().setLngLat(latLng).setDOMContent(mainOfficeDiv).addTo(map);
</script> -->
<footer class="site__footer">
  <div id="footer_widgets"><?php dynamic_sidebar('footer'); ?></div>
  <div id="footer_social">
    <a href="https://www.facebook.com/ReddingChamberofCommerce/"><?php MOZ_SVG::svg('facebook'); ?></a>
    <a href="https://twitter.com/ReddingChamber1"><?php MOZ_SVG::svg('twitter'); ?></a>
    <a href="https://plus.google.com/107595056307311633205/about"><?php MOZ_SVG::svg('google-plus'); ?></a>
    <a href="https://www.instagram.com/explore/tags/reddingchamber/"><?php MOZ_SVG::svg('instagram'); ?></a>
  </div>
</footer>
<footer id="colophon">
  Copyright &copy; 2016 The Greater Redding Chamber of Commerce | <a href="http://pacificsky.co" target="_blank" style="display: -webkit-box;display: -ms-flexbox;display: inline-flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;text-decoration: none;color: inherit;">Design &amp; Development by&nbsp;<img src="https://pacificsky.co/logo?variant=white" style="width:12rem" /></a>
</footer>


<?php wp_footer(); ?>

<?php // </body> opens in header.php ?>
</body>
</html>
