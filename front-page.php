<?php get_header(); ?>

<script charset="utf-8">
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('video_player', {
		height: '390',
		width: '640',
		videoId: 'fifcZFQmWoM',
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}
function onPlayerReady(event) {}
var done = false;
function onPlayerStateChange(event) {}
jQuery('body').on('click', '#chamber_play_video', function() {
	chamberHomeVideoPlay();
})
function chamberHomeVideoPlay () {
	jQuery('#video_player_wrapper').fadeIn(500, function() {
		player.playVideo();
	});
}
jQuery('body').on('click', '#video_player_wrapper', function () {
	player.stopVideo();
	jQuery('#video_player_wrapper').fadeOut(500);
});
</script>
<?php echo do_shortcode('[rev_slider alias="video-slider"]'); ?>
<div id="video_player_wrapper" style="display:none"><div id="video_player"></div></div>


<?php
$premier_partners = get_posts(array(
	'post_type' => 'chamber_partners',
	'posts_per_page' => -1,
	'partner-category' => 'premier'
));
$standard_partners = get_posts(array(
	'post_type' => 'chamber_partners',
	'posts_per_page' => -1,
	'partner-category' => 'standard'
));

$partners = array_merge($premier_partners, $standard_partners);

?>

<main>

	<div id="PAR">
		<div class="slider--wrapper">
			<div class="slider--pane pane--disabled icons">
				<a href="<?php echo get_bloginfo('url'); ?>/partnership" class="partnership">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/partnership.svg" alt="" />
					<h2>Partnership</h2>
				</a>
				<a href="<?php echo get_bloginfo('url'); ?>/resources/business-advocacy" class="advocacy">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/advocacy.svg" alt="" />
					<h2>Advocacy</h2>
				</a>
				<a href="<?php echo get_bloginfo('url'); ?>/resources" class="resources">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/svg/resources.svg" alt="" />
					<h2>Resources</h2>
				</a>
			</div>
			<!-- <div class="slider--pane">Sponsorship</div> -->
		</div>
	</div>

	<!-- <h2 class="divider--double"><span><small>Community</small>Connection</span></h2> -->

	<div id="community_connections">

		<div id="home_events"><h2>Events</h2><?php echo do_shortcode('[chamber_events_feed]'); ?></div>

		<div id="home_news"><?php echo do_shortcode('[widget id="ff_widget-2"]'); ?></div>

	</div>

	<div id="premier_partners">
		<h2><span><!--<a href="https://reddingcacoc.wliinc17.com/join">-->Join Our Champion Business Sponsors<!--</a>--></span></h2>
		<div class="slider--wrapper">
			<?php foreach ($partners as $i => $partner): ?>
				<?php if($i === 0) echo "<div class='slider--pane'>"; ?>
				<figure class="partner">
						<a href="<?php echo get_post_meta($partner->ID, '_chamber_partner_url', true); ?>" target="_blank"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($partner->ID)); ?>" alt="" /></a>
				</figure>
				<?php if(($i + 1) % 3 === 0) {
					echo "</div>";
					if($i + 1 !== count($partners)) echo "<div class='slider--pane' style='display: none;'>";
				} elseif($i + 1 === count($partners)) echo "</div>"; ?>
			<?php endforeach; ?>
		</div>
		<script charset="utf-8">
			jQuery(document).ready(function($) {
				var $panes = $('.slider--pane:not(.pane--disabled)');
				var paneDuration = 5000;
				var paneTransition = 1000;
				var currentPane = 0;
				var nextPane = 1;
				setInterval(function () {
					if(currentPane + 1 === $panes.length) nextPane = 0;
					else nextPane = currentPane + 1;
					$($panes[currentPane]).fadeOut(paneTransition, function () {
						$($panes[nextPane]).fadeIn(paneTransition, function () {
							currentPane = nextPane;
						})
					})
				}, paneDuration);
			});
		</script>
	</div>


	<!-- <div id="standard_partners">

		<?php
		$standard_partners = get_posts(array(
			'post_type' => 'chamber_partners',
			'posts_per_page' => -1,
			'partner-category' => 'standard'
		));
		foreach ($standard_partners as $partner) {
			?>
			<figure class="partner">
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($partner->ID)); ?>" alt="" />
			</figure>
			<?php
		}
		?>
	</div> -->

	<!-- <h2 class="divider--double"><span><small>Featured</small>Member</span></h2> -->

	<?php echo do_shortcode('[chamber_featured_member]'); ?>

	<!--Begin CTCT Sign-Up Form-->
	<!-- EFD 1.0.0 [Wed Jun 01 13:16:32 EDT 2016] -->
	<link rel='stylesheet' type='text/css' href='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/css/signup-form.css'>
	<div class="ctct-embed-signup">
	   <div>
	       <span id="success_message" style="display:none;">
	           <div style="text-align:center;">Thanks for signing up!</div>
	       </span>
	       <form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
	           <h2 style="margin:0;">Subscribe to our newsletter</h2>
	           <!-- The following code must be included to ensure your sign-up form works properly. -->
	           <input data-id="ca:input" name="ca" value="288663f2-341c-48f9-bfef-90f0ffd00484" type="hidden">
	           <input data-id="list:input" name="list" value="2013057163" type="hidden">
	           <input data-id="source:input" name="source" value="EFD" type="hidden">
	           <input data-id="required:input" name="required" value="list,email" type="hidden">
	           <input data-id="url:input" name="url" value="" type="hidden">
	           <p data-id="Email Address:p" ><label data-id="Email Address:label" data-name="email" class="ctct-form-required">Email Address</label> <input data-id="Email Address:input" name="email" value="" maxlength="80" type="text"></p>
	           <button type="submit" data-enabled="enabled">Sign Up</button>
	       </form>
	   </div>
	</div>
	<script type='text/javascript'>
	   var localizedErrMap = {};
	   localizedErrMap['required'] = 		'This field is required.';
	   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
	   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
	   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
	   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
	   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
	   localizedErrMap['list'] = 			'Please select at least one email list.';
	   localizedErrMap['generic'] = 		'This field is invalid.';
	   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
	   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
		localizedErrMap['state_province'] = 'Select a state/province';
	   localizedErrMap['selectcountry'] = 	'Select a country';
	   var postURL = 'https://visitor2.constantcontact.com/api/signup';
	</script>
	<script type='text/javascript' src='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/js/signup-form.js'></script>
	<!--End CTCT Sign-Up Form-->

</main>

<?php get_footer(); ?>
